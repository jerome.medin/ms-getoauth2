import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import okhttp3.*;
import okio.BufferedSink;
import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainFlow {

    private final String ENDPOINT = "login.live.com";
    private final String PATH = "oauth20_authorize.srf";
    private final String REDIRECT_URI = "https://login.live.com/oauth20_desktop.srf";
    private final String RESPONSE_TYPE = "code";

    private final String SCOPES = "onedrive.readwrite offline_access";

    private final OkHttpClient client;

    private final String CLIENT_ID = "ada55230-877d-49b4-971e-76f5b651f4ad";

    public MainFlow() throws MalformedURLException {
        client = new OkHttpClient.Builder().eventListener(new EventListener() {
            @Override
            public void callStart(Call call) {
                System.out.println(call.toString());
            }
        }).build();


        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host(ENDPOINT)
                .addPathSegment(PATH)
                .addEncodedQueryParameter("client_id", CLIENT_ID)
                .addEncodedQueryParameter("redirect_uri", REDIRECT_URI)
                .addEncodedQueryParameter("response_type", RESPONSE_TYPE)
                .addEncodedQueryParameter("scope", SCOPES)
                .build();
//        Request request =new Request.Builder().get()
//                .url(url)
//                .build();

//        System.out.println("Paste into browser: " + request.);

//        final StringBuilder b = new StringBuilder();
//        b.append(request.url().toString()).append("?").append(request.url().encodedQuery());

//        WebView web = new WebView();
//        web.getEngine().load(url.toString());

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JFXPanel panel = new JFXPanel();
        panel.setSize(1280 ,720);
        panel.setBackground(Color.BLUE);
        panel.setOpaque(true);

        frame.setContentPane(panel);

        frame.setLocationRelativeTo(null);
        frame.setSize(1280,720);


        Platform.runLater(() -> {
            System.out.println("WebView");
            WebView web = new WebView();
            web.setMinSize(1280,720);
            web.setVisible(true);
            Group group = new Group();
            group.getChildren().add(web);
            panel.setScene(new Scene(group));
            web.getEngine().load(url.toString());
            frame.setVisible(true);
            web.getEngine().locationProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    System.out.println("Location changed: " + newValue);
                    if (newValue.contains("code=")) {
                        System.out.println("Found code!");

                        Pattern p = Pattern.compile(".*?code=(.*)\\&.*");
                        Matcher matcher = p.matcher(newValue);
                        if (matcher.matches()) {
                            String code = matcher.group(1);
                            System.out.println("Code: " + code);
                            doFinalRequest(code);
                        }
                    }
                }
            });
        });

        System.out.println("Paste into browser: " +url.toString());

//        client.newCall(request).enqueue(new okhttp3.Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                System.out.println(response.toString());
//            }
//        });

    }

    void doFinalRequest(String code) {
        HttpUrl url = new HttpUrl.Builder()
                .host(ENDPOINT)
                .addPathSegment("oauth20_token.srf")
                .scheme("https")
                .build();
        Request req = new Request.Builder()
                .url(url)
                .post(new RequestBody() {
                    @Override
                    public MediaType contentType() {
                        return MediaType.parse("application/x-www-form-urlencoded");
                    }

                    @Override
                    public void writeTo(BufferedSink sink) throws IOException {
                        StringBuilder b = new StringBuilder();
                        b.append("client_id=").append(CLIENT_ID).append("&")
                                .append("redirect_uri=").append(REDIRECT_URI).append("&")
                                .append("code=").append(code).append("&")
                                .append("grant_type=authorization_code");
                        System.out.println("Body: " + b.toString());
                        sink.writeUtf8(b.toString());
                    }
                })
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .build();

        try {
            System.out.println("Request: " + req.toString());
            Response response = client.newCall(req).execute();
            System.out.println(response.toString());
            System.out.println(response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



    interface Callback {
        void onSuccess();
    }

}
